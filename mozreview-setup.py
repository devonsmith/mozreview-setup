#!/usr/bin/python

##############################################################################
##                                                                          ##
## MozReview Setup Script                                                   ##
## Devon Smith                                                              ##
##                                                                          ##
## Additional Contributors:                                                 ##
## Tested By: Aaron Earl                                                    ##
## Research by: Jake H                                                      ##
##                                                                          ##
## CS435: Open Source Software Development                                  ##
## Western Oregon University                                                ##
## Spring 2017                                                              ##
##                                                                          ##
## Purpose: This script is intended to make the setup of the Mozreview      ##
##          mercurial configuration simpler for students in the CS435       ##
##          class. Since there are no relevent instructions on the Mozilla  ##
##          Developer Network for the current                               ##
## Mozreview configuration, this script should do the following:            ##
##                                                                          ##
##      * Install the version control tools from mercurial if they do not   ##
##        exist.                                                            ##
##      * Prompt for the bugzilla username for the user                     ##
##      * Prompt for the user's API Key                                     ##
##      * Setup the ~/.hgrc file for use with Mozilla's Mozreview           ##
##                                                                          ##
##############################################################################

## Note: Python 3 is required for this script.

#Imports of libraries used in this file.
import os  # Operating system tools. for check if exists.


## Main ##
def main():

    # we need to get the user's home path from the system.
    # On linux ~/ represents home.
    home = os.path.expanduser('~')

    # Does the user have the version control tools installed? If not, we're
    # going to need those.
    if os.path.exists(home + '/version-control-tools') is False:
        # Run mercurial and clone the version control tools extension from
        # mozilla.
        os.system('hg clone '
                  + 'https://hg.mozilla.org/hgcustom/version-control-tools'
                  + ' ~/version-control-tools')
        print("\n\n version control tools have been installed!\n\n")
    else:
        print("\n\n Version control tools already installed!\n\n")

    output = '\n'
    # The header of the section in the file
    output += '[bugzilla]'
    # Ask the user for their username
    output += '\nusername = ' + input('Please enter your bugzilla username: ')
    # Ask the user for their API key
    output += '\napikey = ' + input('Please enter your api key: ')
    # Mozilla section header
    output += '\n\n[mozilla]'
    # Aske the user for their irc nickname.
    output += '\nircnick = ' + input('Please enter your IRC nickname: ')
    # Extensions header
    output += '\n\n[extensions]'
    output += '\nmq='
    # Line for the reviewboard location.
    output += ('\nreviewboard = '
               + '~/version-control-tools/hgext/reviewboard/client.py')
    # Header for the paths section
    output += '\n\n[paths]'
    # Locations for the server locations.
    output += '\ndefault = http://hg.mozilla.org/mozilla-central/'
    output += ('\nreview = review = '
               + 'https://reviewboard-hg.mozilla.org/autoreview')

    # This is the file path for the .hgrc file from the user's home directory.
    filePath = home + '/.hgrc'

    # Open the file and place the output string/sequence into the file,
    # appending to the end of the file.
    with open(filePath, 'a') as file:
        file.write(output)

    ##########################################################################
    ##                                                                      ##
    ## Note: Mercurial is tolerant of having multiple sections that         ##
    ##       represent the same set in the configuration file,  This means  ##
    ##       you can have multiple [extensions] headers, for example, and   ##
    ##       mercurial will gracefully handle the duplicates for you. This  ##
    ##       script will merely append to the user-specific hg              ##
    ##       configuration file.                                            ##
    ##                                                                      ##
    ##       The user-specific file has higher precedence than the          ##
    ##       system-wide configuration file and will override all the       ##
    ##       configurations in the system-wide configuration file that      ##
    ##       match.                                                         ##
    ##                                                                      ##
    ##########################################################################

# calls the main function to start the script.
if __name__ == '__main__':
    main()
